import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Subscription } from "rxjs";
import { BreadcrumbsService } from "ng6-breadcrumbs";

@Component({
  selector: "app-admin-panel",
  templateUrl: "./admin-panel.component.html",
  styleUrls: ["./admin-panel.component.scss"],
})
export class AdminPanelComponent implements OnInit {
  titleHeading: any;
  titleDescription: any;
  subscription: Subscription;

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        let child = this.activatedRoute.firstChild;
        this.titleHeading = child.snapshot.data.breadcrumb;
        this.titleDescription = child.snapshot.data.tittleInformation;
      }
    });
  }

  ngOnInit() {}
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
