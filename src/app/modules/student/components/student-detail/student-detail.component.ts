import { DatePipe } from "@angular/common";
import { AssessmentService } from "./../../../assessment/services/assessment.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrMessageService } from "./../../../../shared/services/toastr-message/toastr-message.service";
import { StudentService } from "./../../services/student.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-student-detail",
  templateUrl: "./student-detail.component.html",
  styleUrls: ["./student-detail.component.scss"],
  providers: [DatePipe],
})
export class StudentDetailComponent implements OnInit {
  student_id = this.activatedRoute.snapshot.params.id;
  studentDetail: any;
  constructor(
    private studentService: StudentService,
    private toastrMessageService: ToastrMessageService,
    private activatedRoute: ActivatedRoute,
    private assessmentService: AssessmentService,
    private datePipe: DatePipe,
    private router: Router
  ) {
    this.getStudentById(this.student_id);
  }

  ngOnInit() {
    this.getAssessment();
  }

  // get teacher
  getStudentById(id) {
    this.studentService.getStudentById(id).subscribe(
      (response) => {
        if (response) {
          this.studentDetail = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  checkDate(date) {
    let currentDate = new Date().getDate();
    let startDate = new Date(date).getDate();
    if (startDate > currentDate) {
      return true;
    } else {
      return false;
    }
  }

  onSubmit(dataItem) {
    let body = {
      id: dataItem.id,
      score: dataItem.score,
      startDate: dataItem.startDate,
      student: dataItem.student,
      submittedDate: this.datePipe.transform(new Date(), "yyyy-MM-dd"),
      title: dataItem.title,
      verified: dataItem.verified,
    };
    this.updateAssessment(body);
  }

  assessmentList = [];
  // get assessment
  getAssessment() {
    this.assessmentService.getAssessment().subscribe(
      (response) => {
        if (response) {
          response.forEach((item) => {
            if (this.student_id == item.student) {
              this.assessmentList.push(item);
            }
          });
          console.log(this.assessmentList);
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  updateAssessment(assessment) {
    this.assessmentService.updateAssessment(assessment).subscribe(
      () => {
        this.toastrMessageService.showSuccess(
          "Assessment Submited Successfully"
        );

        this.getAssessment();
        this.router.navigate(["/admin"]);
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }
}
/**
 * code written by
 * Suraj kumar Regmi
 *
 */
