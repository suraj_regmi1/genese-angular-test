import { environment } from "@env/environment";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class TeacherService {
  private _url = environment.base_api_url;
  constructor(private http: HttpClient) {}

  getTeachers(): Observable<any> {
    return this.http.get(`${this._url}/teachers`);
  }

  getTeacherById(id): Observable<any> {
    return this.http.get(`${this._url}/teachers/${id}`);
  }
}
