import { SharedModule } from "@app/shared/shared.module";
import { StudentRoutingModule } from "./student-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentComponent } from "./components/student.component";
import { StudentDetailComponent } from './components/student-detail/student-detail.component';

@NgModule({
  declarations: [StudentComponent, StudentDetailComponent],
  imports: [CommonModule, StudentRoutingModule, SharedModule],
})
export class StudentModule {}
