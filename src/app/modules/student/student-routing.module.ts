import { StudentDetailComponent } from "./components/student-detail/student-detail.component";
import { StudentComponent } from "./components/student.component";

import { NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: StudentComponent },
  {
    path: "detail/:id",
    component: StudentDetailComponent,
    data: {
      breadcrumb: "Student Detail",
      tittleInformation: "Control Panel for Student.",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentRoutingModule {}
