import { environment } from "@env/environment";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class StudentService {
  private _url = environment.base_api_url;
  constructor(private http: HttpClient) {}

  getStudents(): Observable<any> {
    return this.http.get(`${this._url}/students`);
  }

  getStudentById(id): Observable<any> {
    return this.http.get(`${this._url}/students/${id}`);
  }
}
