import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TopbarComponent } from "./admin-panel/topbar/topbar.component";
import { SidebarComponent } from "./admin-panel/sidebar/sidebar.component";

import { AdminPanelComponent } from "./admin-panel/admin-panel.component";
import { RouterModule } from "@angular/router";
import { MenuItemComponent } from "./admin-panel/sidebar/menu-item/menu-item.component";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ConfirmationDialogComponent } from "@app/shared/components/confirmation-dialog/confirmation-dialog.component";
import { SharedModule } from "@app/shared/shared.module";
import { BreadcrumbsModule } from "ng6-breadcrumbs";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  declarations: [
    AdminPanelComponent,
    TopbarComponent,
    SidebarComponent,

    MenuItemComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,

    BrowserAnimationsModule,
    SharedModule,
    BreadcrumbsModule,
    Ng2SearchPipeModule,
  ],
  entryComponents: [ConfirmationDialogComponent],
  exports: [ConfirmationDialogComponent],
})
export class LayoutModule {}
