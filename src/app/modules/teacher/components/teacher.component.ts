import { Router } from "@angular/router";
import { ToastrMessageService } from "./../../../shared/services/toastr-message/toastr-message.service";
import { RegexConst } from "./../../../shared/constants/regex.constant";
import { Validators, AbstractControl } from "@angular/forms";
import { FormGroup, FormBuilder } from "@angular/forms";
import { TeacherService } from "./../services/teacher.service";
import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
@Component({
  selector: "app-teacher",
  templateUrl: "./teacher.component.html",
  styleUrls: ["./teacher.component.scss"],
})
export class TeacherComponent implements OnInit {
  modalRef: BsModalRef;
  regex = RegexConst;
  teacherList: any;
  constructor(
    private teacherService: TeacherService,
    private modalService: BsModalService,
    private fb: FormBuilder,
    private toastrMessageService: ToastrMessageService,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildTeacherIdForm();
    this.getTeacherList();
  }

  teacherIdForm: FormGroup;
  buildTeacherIdForm() {
    this.teacherIdForm = this.fb.group({
      teacher_id: [
        "",
        [Validators.required, Validators.pattern(this.regex.NUMBER)],
      ],
    });
  }

  // get teacher
  getTeacherList() {
    this.teacherService.getTeachers().subscribe(
      (response) => {
        if (response) {
          this.teacherList = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  isteacherId: boolean;
  onLogIn() {
    if (this.teacherIdForm.invalid) return;

    for (let i = 0; i < this.teacherList.length; i++) {
      if (
        this.teacherList[i].id == parseInt(this.teacherIdForm.value.teacher_id)
      ) {
        this.isteacherId = true;
        break;
      } else {
        this.isteacherId = false;
      }
    }

    if (this.isteacherId) {
      this.toastrMessageService.showSuccess("Teacher logged in successfully");

      this.router.navigate([
        "/teacher/detail",
        parseInt(this.teacherIdForm.value.teacher_id),
      ]);
      // navigate to teacher detail
    } else {
      this.toastrMessageService.showError("Teacher Id doesnot match");
    }
  }
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
