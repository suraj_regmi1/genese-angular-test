import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { Router } from "@angular/router";
declare var $: any;
@Component({
  selector: "menu-item",
  templateUrl: "./menu-item.component.html",
  styleUrls: ["./menu-item.component.scss"],
})
export class MenuItemComponent implements OnInit {
  @Input() navItems;

  expanded: boolean;
  term: any;
  constructor(private router: Router) {}

  ngOnInit() {
    $(document).ready(() => {
      const trees: any = $('[data-widget="tree"]');
      if (trees) {
        trees.tree();
      }
    });
  }

  setExpandedTrue(event) {
    if (event.target.value != "") {
      this.expanded = true;
    } else {
      this.expanded = false;
    }
  }
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
