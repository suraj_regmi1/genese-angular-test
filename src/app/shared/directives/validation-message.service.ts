import { Injectable } from "@angular/core";

@Injectable()
export class ValidationMsgService {
  public getValidationMsg(valErrors, inputLabelName: string) {
    const firstKey = Object.keys(valErrors)[0];
    // if (!inputLabelName || inputLabelName == undefined) {
    //   inputLabelName = "This field";
    // }

    if (firstKey == "required") {
      return inputLabelName + " is Required";
    } else if (firstKey == "minlength") {
      return (
        inputLabelName +
        " must have " +
        valErrors.minlength.requiredLength +
        " characters"
      );
    } else if (firstKey == "maxlength") {
      return (
        inputLabelName +
        " cannot exceed " +
        valErrors.maxlength.requiredLength +
        " characters"
      );
    } else if (firstKey == "pattern") {
      return inputLabelName + " is Invalid";
    } else if (firstKey == "idCheckValidator") {
      return inputLabelName + "Does not match";
    } else {
      return "Invalid Input";
    }
  }
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
