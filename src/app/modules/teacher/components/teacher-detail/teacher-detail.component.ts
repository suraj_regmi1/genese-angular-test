import { AdminService } from "./../../../admin/services/admin.service";
import { RegexConst } from "./../../../../shared/constants/regex.constant";
import { Validators } from "@angular/forms";
import { FormGroup, FormBuilder } from "@angular/forms";
import { StudentService } from "./../../../student/services/student.service";
import { AssessmentService } from "./../../../assessment/services/assessment.service";
import { ToastrMessageService } from "./../../../../shared/services/toastr-message/toastr-message.service";
import { TeacherService } from "./../../services/teacher.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-teacher-detail",
  templateUrl: "./teacher-detail.component.html",
  styleUrls: ["./teacher-detail.component.scss"],
  providers: [DatePipe],
})
export class TeacherDetailComponent implements OnInit {
  teacher_id = this.activatedRoute.snapshot.params.id;
  teacherDetail: any;
  modalRef: BsModalRef;

  regex = RegexConst;
  constructor(
    private activatedRoute: ActivatedRoute,
    private teacherService: TeacherService,
    private toastrMessageService: ToastrMessageService,
    private assessmentService: AssessmentService,
    private modalService: BsModalService,
    private studentService: StudentService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private adminService: AdminService,
    private router: Router
  ) {
    this.getTeacherById(this.teacher_id);
    this.getStudentList();
  }

  ngOnInit() {
    this.getAssessment();
    this.buildAssessmentForm();
  }

  assessmentForm: FormGroup;
  buildAssessmentForm() {
    this.assessmentForm = this.fb.group({
      title: [
        this.selectedAssessment ? this.selectedAssessment.title : "",
        Validators.required,
      ],
      score: [
        this.selectedAssessment ? this.selectedAssessment.score : "",
        [Validators.required, Validators.pattern(this.regex.NUMBER)],
      ],
      startDate: [
        this.selectedAssessment ? this.selectedAssessment.startDate : "",
        Validators.required,
      ],
      verified: [
        this.selectedAssessment ? this.selectedAssessment.verified : false,
      ],
      student: [
        this.selectedAssessment ? this.selectedAssessment.student : "",
        Validators.required,
      ],
      submittedDate: [
        this.selectedAssessment ? this.selectedAssessment.submittedDate : "",
      ],
    });
  }

  // get teacher
  getTeacherById(id) {
    this.teacherService.getTeacherById(id).subscribe(
      (response) => {
        if (response) {
          this.teacherDetail = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }
  assessmentList: any;
  // get assessment
  getAssessment() {
    this.assessmentService.getAssessment().subscribe(
      (response) => {
        if (response) {
          this.assessmentList = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  // add teacher...
  addAssessment(assessment) {
    this.assessmentService.addAssessment(assessment).subscribe(
      (response) => {
        if (response) {
          this.toastrMessageService.showSuccess(
            "Assessment Added Successfully"
          );
        }
        this.modalRef.hide();
        this.getAssessment();
        this.assessmentForm.reset();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  updateAssessment(assessment) {
    this.assessmentService.updateAssessment(assessment).subscribe(
      () => {
        this.toastrMessageService.showSuccess(
          "Assessment Updated Successfully"
        );
        this.modalRef.hide();
        this.getAssessment();
        this.assessmentForm.reset();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  studentList: any;

  // get student
  getStudentList() {
    this.studentService.getStudents().subscribe(
      (response) => {
        if (response) {
          this.studentList = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };

  editMode: boolean;
  modalTitle: string;
  selectedAssessment: any;
  openAssessmentEditModal(template: TemplateRef<any>, dataItem): void {
    this.editMode = true;
    this.modalTitle = "Edit";
    this.selectedAssessment = dataItem;
    this.buildAssessmentForm();
    this.modalRef = this.modalService.show(template, this.config);
  }

  openAssessmentAddModal(template: TemplateRef<any>): void {
    this.editMode = false;
    this.modalTitle = "Add";
    this.selectedAssessment = null;
    this.buildAssessmentForm();
    this.modalRef = this.modalService.show(template, this.config);
  }
  onToggleChange(event) {
    this.assessmentForm.patchValue({
      verified: event,
    });
  }
  onVerify(dataItem) {
    let body = {
      id: dataItem.id,
      score: dataItem.score,
      startDate: dataItem.startDate,
      student: dataItem.student,
      submittedDate: dataItem.submittedDate,
      title: dataItem.title,
      verified: true,
    };

    this.updateAssessment(body);
    this.getAssessment();
  }

  onSubmitAssessment() {
    if (this.assessmentForm.invalid) return;

    this.assessmentForm.value.startDate = this.datePipe.transform(
      this.assessmentForm.value.startDate,
      "yyyy-MM-dd"
    );

    if (this.editMode) {
      this.assessmentForm.value["id"] = this.selectedAssessment.id;
      console.log(this.assessmentForm.value);
      this.updateAssessment(this.assessmentForm.value);
    } else {
      console.log(this.assessmentForm.value);
      this.addAssessment(this.assessmentForm.value);
    }
  }
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
