import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "@env/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class AssessmentService {
  private _url = environment.base_api_url;
  constructor(private http: HttpClient) {}

  getAssessment(): Observable<any> {
    return this.http.get(`${this._url}/assessments`);
  }

  addAssessment(student): Observable<any> {
    return this.http.post(`${this._url}/assessments`, student, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    });
  }

  updateAssessment(assessment): Observable<void> {
    return this.http.put<void>(
      `${this._url}/assessments/${assessment.id}`,
      assessment,
      {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
        }),
      }
    );
  }
}
