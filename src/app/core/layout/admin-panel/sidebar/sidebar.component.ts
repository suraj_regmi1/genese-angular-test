import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";
import { environment } from "@env/environment";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
  defaultImagePath = environment.defaultImagePath;

  navItems = [
    {
      id: 1,
      displayName: "Admin",
      iconName: "fa fa-dashboard",
      route: "/admin",
    },
    {
      id: 2,
      displayName: "Teacher",
      iconName: "fa fa-th",
      route: "/teacher",
    },
    {
      id: 2,
      displayName: "Student",
      iconName: "fa fa-th",
      route: "/student",
    },
  ];
  constructor() {}

  ngOnInit() {}

  toogleSideNav() {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
  }
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
