import { Router } from "@angular/router";
import { ToastrMessageService } from "./../../../shared/services/toastr-message/toastr-message.service";
import { StudentService } from "./../services/student.service";
import { RegexConst } from "./../../../shared/constants/regex.constant";
import { Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-student",
  templateUrl: "./student.component.html",
  styleUrls: ["./student.component.scss"],
})
export class StudentComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private studentService: StudentService,
    private toastrMessageService: ToastrMessageService,
    private router: Router
  ) {
    this.getStudentList();
  }
  regex = RegexConst;
  studentIdForm: FormGroup;
  ngOnInit() {
    this.buildStudentIdForm();
  }

  buildStudentIdForm(): void {
    this.studentIdForm = this.fb.group({
      student_id: [
        "",
        [Validators.required, Validators.pattern(this.regex.NUMBER)],
      ],
    });
  }
  studentList: any;
  // get student
  getStudentList() {
    this.studentService.getStudents().subscribe(
      (response) => {
        if (response) {
          this.studentList = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }
  isStudentId: boolean;
  onLogIn() {
    if (this.studentIdForm.invalid) return;

    for (let i = 0; i < this.studentList.length; i++) {
      if (
        this.studentList[i].id == parseInt(this.studentIdForm.value.student_id)
      ) {
        this.isStudentId = true;
        break;
      } else {
        this.isStudentId = false;
      }
    }

    if (this.isStudentId) {
      this.toastrMessageService.showSuccess("Student logged in successfully");

      this.router.navigate([
        "/student/detail",
        parseInt(this.studentIdForm.value.student_id),
      ]);
      // navigate to teacher detail
    } else {
      this.toastrMessageService.showError("Student Id doesnot match");
    }
  }
}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
