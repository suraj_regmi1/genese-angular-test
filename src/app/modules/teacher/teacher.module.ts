import { SharedModule } from "./../../shared/shared.module";
import { TeacherRoutingModule } from "./teacher-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TeacherComponent } from "./components/teacher.component";
import { TeacherDetailComponent } from "./components/teacher-detail/teacher-detail.component";
import { UiSwitchModule } from "ngx-toggle-switch";
@NgModule({
  declarations: [TeacherComponent, TeacherDetailComponent],
  imports: [CommonModule, TeacherRoutingModule, SharedModule, UiSwitchModule],
})
export class TeacherModule {}
