import { TeacherDetailComponent } from "./components/teacher-detail/teacher-detail.component";
import { TeacherComponent } from "./components/teacher.component";

import { NgModule } from "@angular/core";

import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: TeacherComponent },
  {
    path: "detail/:id",
    component: TeacherDetailComponent,
    data: {
      breadcrumb: "Teacher Detail",
      tittleInformation: "Control Panel for Teacher.",
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeacherRoutingModule {}
