import { ConfirmationDialogComponent } from "./../../../shared/components/confirmation-dialog/confirmation-dialog.component";
import { AdminService } from "./../services/admin.service";
import { RegexConst } from "./../../../shared/constants/regex.constant";
import { FormBuilder, Validators } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { StudentService } from "./../../student/services/student.service";
import { ToastrMessageService } from "./../../../shared/services/toastr-message/toastr-message.service";
import { TeacherService } from "./../../teacher/services/teacher.service";
import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.scss"],
})
export class AdminComponent implements OnInit {
  modalRef: BsModalRef;
  teacherList: any;
  teacherForm: FormGroup;
  studentForm: FormGroup;
  regex = RegexConst;
  selectedTeacher: any;
  selectedStudent: any;

  editMode: boolean;

  submitButton: string;
  modalTitle: string;
  constructor(
    private modalService: BsModalService,
    private teacherService: TeacherService,
    private toastrMessageService: ToastrMessageService,
    private studentService: StudentService,
    private fb: FormBuilder,
    private adminService: AdminService
  ) {}

  ngOnInit() {
    this.getTeacherList();
    this.getStudentList();
    this.buildTeacherForm();
    this.buildStudentForm();
  }

  buildTeacherForm() {
    this.teacherForm = this.fb.group({
      name: [
        this.selectedTeacher ? this.selectedTeacher.name : "",
        Validators.required,
      ],
      class: [
        this.selectedTeacher ? this.selectedTeacher.class : "",
        Validators.required,
      ],
    });
  }

  buildStudentForm() {
    this.studentForm = this.fb.group({
      name: [
        this.selectedStudent ? this.selectedStudent.name : "",
        Validators.required,
      ],
      score: [
        this.selectedStudent ? this.selectedStudent.score : "",
        [Validators.required, Validators.pattern(this.regex.NUMBER)],
      ],
    });
  }

  // get teacher
  getTeacherList() {
    this.teacherService.getTeachers().subscribe(
      (response) => {
        if (response) {
          this.teacherList = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }
  studentList: any;

  // get student
  getStudentList() {
    this.studentService.getStudents().subscribe(
      (response) => {
        if (response) {
          this.studentList = response;
        }
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }
  // add teacher...
  addTeacher(teacher) {
    this.adminService.addTeacher(teacher).subscribe(
      (response) => {
        if (response) {
          this.toastrMessageService.showSuccess("Teacher Added Successfully");
        }
        this.modalRef.hide();
        this.getTeacherList();
        this.teacherForm.reset();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  updateTeacher(teacher) {
    this.adminService.updateTeacher(teacher).subscribe(
      () => {
        this.toastrMessageService.showSuccess("Teacher Updated Successfully");
        this.modalRef.hide();
        this.getTeacherList();
        this.teacherForm.reset();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  // delete teacher

  deleteTeacherById(id: number): void {
    this.adminService.deleteTeacher(id).subscribe(
      () => {
        this.toastrMessageService.showSuccess("Teacher Deleted successfully");
        this.getTeacherList();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  addStudent(student) {
    this.adminService.addStudent(student).subscribe(
      (response) => {
        if (response) {
          this.toastrMessageService.showSuccess("Student Added Successfully");
        }
        this.modalRef.hide();
        this.getStudentList();
        this.teacherForm.reset();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  updateStudent(student) {
    this.adminService.updatestudent(student).subscribe(
      () => {
        this.toastrMessageService.showSuccess("Student Updated Successfully");
        this.modalRef.hide();
        this.getStudentList();
        this.teacherForm.reset();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  deleteStudentById(id: number): void {
    this.adminService.deletestudent(id).subscribe(
      () => {
        this.toastrMessageService.showSuccess("Student Deleted successfully");
        this.getStudentList();
      },
      (error) => {
        this.toastrMessageService.showError(error);
      }
    );
  }

  // modal config
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
  };

  openTeacherAddModal(template: TemplateRef<any>): void {
    this.editMode = false;
    this.modalTitle = "Add";
    this.selectedTeacher = null;
    this.buildTeacherForm();
    this.modalRef = this.modalService.show(template, this.config);
  }

  openTeacherEditModal(template: TemplateRef<any>, selectedTeacher): void {
    this.editMode = true;
    this.modalTitle = "Edit";
    this.selectedTeacher = selectedTeacher;
    this.buildTeacherForm();
    this.modalRef = this.modalService.show(template, this.config);
  }

  openStudentAddModal(template: TemplateRef<any>): void {
    this.editMode = false;
    this.modalTitle = "Add";
    this.selectedStudent = null;
    this.buildStudentForm();
    this.modalRef = this.modalService.show(template, this.config);
  }

  openStudentEditModal(template: TemplateRef<any>, selectedStudent): void {
    this.editMode = true;
    this.modalTitle = "Edit";
    this.selectedStudent = selectedStudent;
    this.buildStudentForm();
    this.modalRef = this.modalService.show(template, this.config);
  }

  openConfirmationDialogue(dataItem, modalType): void {
    console.log(modalType);
    this.modalRef = this.modalService.show(ConfirmationDialogComponent);
    this.modalRef.content.data = dataItem.name;
    this.modalRef.content.action = "delete";
    this.modalRef.content.onClose.subscribe((confirm) => {
      if (confirm) {
        if (modalType == "Student") {
          // student delete code here
          this.deleteStudentById(dataItem.id);
        } else {
          this.deleteTeacherById(dataItem.id);
        }
      }
    });
  }

  onSubmitTeacher() {
    if (this.teacherForm.invalid) return;

    if (this.editMode) {
      this.teacherForm.value["id"] = this.selectedTeacher.id;
      console.log(this.teacherForm.value);
      this.updateTeacher(this.teacherForm.value);
    } else {
      this.addTeacher(this.teacherForm.value);
    }
  }

  onSubmitStudent() {
    if (this.studentForm.invalid) return;

    if (this.editMode) {
      this.studentForm.value["id"] = this.selectedStudent.id;
      this.updateStudent(this.studentForm.value);
    } else {
      this.addStudent(this.studentForm.value);
    }
  }
}
/**
 * code written by
 * Suraj kumar Regmi
 *
 */
