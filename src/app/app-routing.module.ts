import { AdminPanelComponent } from "./core/layout/admin-panel/admin-panel.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: AdminPanelComponent,

    data: {
      breadcrumb: "Home",
    },
    children: [
      { path: "", redirectTo: "admin", pathMatch: "full" },
      {
        path: "admin",
        loadChildren: "@modules/admin/admin.module#AdminModule",
        data: {
          breadcrumb: "Admin",
          tittleInformation: "Control Panel for Admin.",
        },
      },

      {
        path: "teacher",
        loadChildren: "@modules/teacher/teacher.module#TeacherModule",
        data: {
          breadcrumb: "Teacher",
          tittleInformation: "Control Panel for Teacher.",
        },
      },
      {
        path: "student",
        loadChildren: "@modules/student/student.module#StudentModule",
        data: {
          breadcrumb: "Student",
          tittleInformation: "Control Panel for Student.",
        },
      },
      {
        path: "assessment",
        loadChildren: "@modules/assessment/assessment.module#AssessmentModule",
        data: {
          breadcrumb: "Assessments",
          tittleInformation: "Control Panel for Assessments.",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

/**
 * code written by
 * Suraj kumar Regmi
 *
 */
