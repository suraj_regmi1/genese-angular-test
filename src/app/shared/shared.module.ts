import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ModalModule, BsDatepickerModule } from "ngx-bootstrap";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// validation directive.....
import { FormControlValidationMsgDirective } from "./directives/validators/validation-message.directive";
import { FormSubmitValidationMsgDirective } from "./directives/validators/submit-validation-msg.directive";
import { ValidationMsgService } from "./directives/validation-message.service";

import { InputsModule } from "@progress/kendo-angular-inputs";

@NgModule({
  declarations: [
    FormControlValidationMsgDirective,
    FormSubmitValidationMsgDirective,
  ],
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    InputsModule,
  ],
  exports: [
    ModalModule,
    Ng2SearchPipeModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    FormControlValidationMsgDirective,
    FormSubmitValidationMsgDirective,
    InputsModule,
  ],
  providers: [ValidationMsgService],
})
export class SharedModule {}
