import { AssessmentRoutingModule } from "./assessment-routing.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AssessmentComponent } from "./components/assessment.component";

@NgModule({
  declarations: [AssessmentComponent],
  imports: [CommonModule, AssessmentRoutingModule],
})
export class AssessmentModule {}
