import { environment } from "@env/environment";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class AdminService {
  private _url = environment.base_api_url;
  constructor(private http: HttpClient) {}

  // teachers service section.

  addTeacher(teacher): Observable<any> {
    return this.http.post(`${this._url}/teachers`, teacher, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    });
  }

  updateTeacher(teacher): Observable<void> {
    return this.http.put<void>(`${this._url}/teachers/${teacher.id}`, teacher, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    });
  }

  deleteTeacher(teacherId: number): Observable<void> {
    return this.http.delete<void>(`${this._url}/teachers/${teacherId}`);
  }

  // student service section

  addStudent(student): Observable<any> {
    return this.http.post(`${this._url}/students`, student, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    });
  }

  updatestudent(student): Observable<void> {
    return this.http.put<void>(`${this._url}/students/${student.id}`, student, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    });
  }

  deletestudent(studentId: number): Observable<void> {
    return this.http.delete<void>(`${this._url}/students/${studentId}`);
  }
}
